from flask import Flask,redirect,url_for,render_template,request, session, make_response, jsonify

from flask_mysqldb import MySQL
import MySQLdb.cursors

import re

app = Flask(__name__, static_url_path="/static", static_folder='D:/Project P/Belajar/e_commerce/static')


app.secret_key = b'\xf7N\xca\x19\xb1\xb7|3'

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'e_commerce'

mysql = MySQL(app)



@app.route('/')
def guest():
    return render_template('/index.html')





@app.route('/user/',methods = ['GET','POST'])
def login():
    error = None
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:

        username = request.form['username']
        password = request.form['password']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM user WHERE username = %s AND password = %s', (username, password,))
        user = cursor.fetchone()
    
        if user:
            session['loggedin'] = True
            session['id_user'] = user['id_user']
            session['username'] = user['username']
            session['password'] = user['password']
            return redirect(url_for('home'))
        else:
            error = 'Username/password tidak tepat,silahkan coba lagi'
    return render_template('/login.html', error = error)





@app.route('/user/register', methods = ['GET', 'POST'])
def register():
    error = None
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'nama' in request.form and 'gender' in request.form and 'alamat' in request.form and 'email' in request.form and 'no_telepon' in request.form :

        username = request.form['username']
        password = request.form['password']
        nama = request.form['nama']
        gender = request.form['gender']
        alamat = request.form['alamat']
        email = request.form['email']
        no_telepon = request.form['no_telepon']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM user WHERE username = %s', (username,))
        user = cursor.fetchone()
        
        if user:
            error = 'Username sudah dipakai!'
        elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
            error = 'Email yang dimasukkan salah!'
        elif not re.match(r'[A-Za-z0-9]+', username):
            error = 'Username harus menggunakan huruf dan angka!'
        elif not username or not password or not email or not no_telepon:
            error = 'Ada form yang belum diisi!'
        else:
            cursor.execute('INSERT INTO user VALUES (NULL, %s, %s, %s, %s, %s, %s,%s)', (username, password, nama, gender, alamat, email,no_telepon, ))
            mysql.connection.commit()
            error = 'Account berhasil dibuat'


    elif request.method == 'POST':
        error = 'Ada form yang belum diisi!'
    return render_template('/register.html', error = error)

@app.route('/user/home')
def home():
    return render_template('/home.html')



@app.route('/kategori')
def kategori():
    return render_template('/kategori.html')



@app.route('/transaksi')
def transaksi():
    return render_template('/transaksi.html')


@app.route('/upload',methods = ['GET', 'POST'])
def upload():
    return render_template('/upload.html')
    

@app.errorhandler(404)
def not_found(error):
    resp = make_response(render_template('error.html'), 404)
    resp.headers['ERROR 404'] = 'A value'
    return resp


if __name__ == '__main__':
    app.run(debug=True)
